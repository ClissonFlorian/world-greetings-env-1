
|                                          | Malformed sem version (warning?) | Version changes | version did not change |      |
| ---------------------------------------- | -------------------------------- | --------------- | ---------------------- | ---- |
| Version From File                        | WARN                             |                 |                        |      |
| Version From Variable                    | WARN                             |                 |                        |      |
| Version From Image Monitor               | WARN                             |                 |                        |      |
| CD_STRATEGY = deliver_to_staging         |                                  |                 |                        |      |
| CD_STRATEGY = deploy_to_production       |                                  |                 |                        |      |
| Change in Package manifest causes update |                                  |                 |                        |      |

